import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TestaConta01 {
	public static void main(String[] args) {
		Cliente cli1 = new Cliente("Pedro", true, "123", 10);
		Cliente cli2 = new Cliente("Rafa", true, "abc", 15);
		Cliente cli3 = new Cliente("Bruno", true, "hjk", 20);
		Cliente cli4 = new Cliente("Lucas", true, "kkk", 5);
		Cliente cli5 = new Cliente("João", true, "456", 8);
		Cliente cli6 = new Cliente("Guilherme", true, "789", 16);
		Cliente cli7 = new Cliente("Amanda", true, "101", 14);
		Cliente cli8 = new Cliente("Luiza", true, "112", 3);
		Cliente cli9 = new Cliente("Caio", true, "113", 30);
		Cliente cli10 = new Cliente("Carlos", true, "114", 50);
		
		List<Cliente> clientes = Arrays.asList(cli1, cli2, cli3, cli4, cli5, cli6, cli7, cli8, cli9, cli10);
		

		 Optional<Cliente> stream1 = clientes.stream()
				.max(Comparator.comparing(Cliente :: getcompras));
		 Optional<Cliente> stream2 = clientes.stream()
				 .min(Comparator.comparing(Cliente :: getcompras));
		 long stream3 = clientes.stream()
				 .count();
		 int sum = clientes.stream()
				 .mapToInt(c -> c.getcompras())
				 .sum();
				 
		 double media = sum / stream3;
				 
		 
		System.out.println("O maior número de compras foi feito pelo " + stream1.get());
		System.out.println("O menor número de compras foi feito pelo " + stream2.get());
		System.out.println("A média de compras feitas pelos clientes foi " + media);
	}

}