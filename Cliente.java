public class Cliente {
	private String nome;
	private boolean status;
	private String senha;
	private int compras;
	
	public Cliente(String nome, boolean status, String senha, int compras) {
		this.nome = nome;
		this.status = status;
		this.senha = senha;
		this.compras = compras;
	}
	public String getnome() {
		return nome;
	}
	public void setnome() {
		this.nome = nome;
	}
	public boolean status() {
		return status;
	}
	public void setstatus(boolean status) {
		this.status = status;
	}
	public String getsenha() {
		return senha;
	}
	public void setsenha(String senha) {
		this.senha = senha;
	}
	public int getcompras() {
		return this.compras;
	}	
	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", status=" + status + ", senha=" + senha + ", compras=" + compras + "]";
	}
		}